package com.games.config;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class AsyncConfig {

	@Value("${max.thread.pool.size}")
	int MAX_POOL_SIZE_1;
	@Value("${max.core.pool.size}")
	int MAX_CORE_POOL_THREADS;
	@Value("${pool.thread.name}")
	String threadName;

	@Bean("gameAutomationAsyncExec")
	public Executor asycExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(MAX_CORE_POOL_THREADS);
		executor.setMaxPoolSize(MAX_POOL_SIZE_1);
		//executor.setQueueCapacity(50);
		executor.setThreadNamePrefix(threadName);
		return executor;
	}

}
