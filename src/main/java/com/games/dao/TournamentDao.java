package com.games.dao;

import java.util.List;

import com.games.model.AutoTournmentMaster;
import com.games.model.LiveTournmentMaster;
import com.games.model.NewUserTournmentMaster;
import com.games.model.RankingMaster;
import com.games.model.TournamentMaster;

public interface TournamentDao {

	// public boolean hourlyTournamentExist(String startTime,String endTime);

	public boolean hourlyNewUserTournamentExist(String startTime, int entryFeeType);

	// public void generateNewUserTournament(String startTime, String endTime,int
	// entryFeeType);

	public void generateNewUserTournament(String startTime, String endTime,
			NewUserTournmentMaster newUserTournmentMaster);

	public TournamentMaster getFormatDetails(int formatId);

	public int saveTournamentDetails(TournamentMaster tournamentMaster);

	public List<RankingMaster> getRankingListByFormatId(int formatId);

	public void saveRankingDetails(RankingMaster rankingMaster);

	public void createTournment(int gameId, int formatId, int entryFeeType, String startTime, String endTime);

	//public List<EmergencyTournmentMaster> getListEmergencyTournments();

//		public boolean checkEmergencyTournmentRequired(EmergencyTournmentMaster emergencyTournmentMaster);

	public List<AutoTournmentMaster> getListAutoTournments(int gameId);
	public LiveTournmentMaster getLastLiveTournmentByGameIdAndFormatId(int gameId,int formatId, int entryFeeType,long tLevelId) ;
	boolean checkGameActive(int gameId);
	public void createAutoTournment(TournamentMaster tournamentMaster);
	
	public int getTournamentRegistrationCount(long tournament_id);
	
	

}
