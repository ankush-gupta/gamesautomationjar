package com.games.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.games.model.AutoTournmentMaster;
import com.games.model.LiveTournmentMaster;
import com.games.model.NewUserTournmentMaster;
import com.games.model.RankingMaster;
import com.games.model.TournamentMaster;
import com.games.util.CommonConstants;

@Repository
public class TournamentDaoImpl implements TournamentDao {
	private static final Logger LOGS = LoggerFactory.getLogger(TournamentDaoImpl.class);

	@Autowired
	@Qualifier("primaryDB")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	
	@Override
	public boolean hourlyNewUserTournamentExist(String startTime, int entryFeeType) {
		return jdbcTemplate.queryForObject(
				"select count(1) from tournaments where start_time=? and entry_fee_type=? and status=?",
				new Object[] { startTime, entryFeeType, CommonConstants.NEW_USER_TOURNMENT_STATUS }, Integer.class) > 0;
	}

	
	@Override
	public void generateNewUserTournament(String startTime, String endTime,
			NewUserTournmentMaster newUserTournmentMaster) {

		final String sqlQuery = "INSERT INTO tournaments	(game_id,name,users,prize_money,prize_money_cash,prize_money_token,entry_fee, "
				+ " entry_fee_cash,entry_fee_token,entry_fee_type,percent_text,status,start_time,end_time,added_date,created,modified,score_flag) "
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_date,now(),now(),1)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement pstmt = connection.prepareStatement(sqlQuery, new String[] { "id" });
				pstmt.setInt(1, newUserTournmentMaster.getGameId());
				pstmt.setString(2, newUserTournmentMaster.getName());
				pstmt.setInt(3, newUserTournmentMaster.getUsers());
				pstmt.setString(4, newUserTournmentMaster.getPrizeMoney());
				pstmt.setInt(5, newUserTournmentMaster.getPrizeMoneyCash());
				pstmt.setInt(6, newUserTournmentMaster.getPrizeMoneyToken());
				pstmt.setString(7, newUserTournmentMaster.getEntryFee());
				pstmt.setInt(8, newUserTournmentMaster.getEntryFeeCash());
				pstmt.setInt(9, newUserTournmentMaster.getEntryFeeToken());
				pstmt.setInt(10, newUserTournmentMaster.getEntryFeeType());
				pstmt.setString(11, newUserTournmentMaster.getPercentText());
				pstmt.setInt(12, CommonConstants.NEW_USER_TOURNMENT_STATUS);
				pstmt.setString(13, startTime);
				pstmt.setString(14, endTime);

				return pstmt;
			}
		}, keyHolder);

		int tournmentId = keyHolder.getKey().intValue();
		if (tournmentId > 0) {

			String sql = "INSERT INTO rank_ranges(tournament_id,max_rank,min_rank,prize_money,prize_money_cash,prize_money_token,status,added_date,created)"
					+ " VALUES(?,?,?,?,?,?,1,current_date,now())";

			for (int i = 0; i < newUserTournmentMaster.getMinRank().size(); i++) {
				jdbcTemplate.update(sql, new Object[] { tournmentId, newUserTournmentMaster.getMaxRank().get(i),
						newUserTournmentMaster.getMinRank().get(i), newUserTournmentMaster.getRankPrizeMoney().get(i),
						newUserTournmentMaster.getRankPrizeMoneyCash().get(i),
						newUserTournmentMaster.getRankprizeMoneyToken().get(i) });
			}

		}
	}

	@Override
	public TournamentMaster getFormatDetails(int formatId) {

		String SEL_QRY = "SELECT format,name,entry_fee_type,entry_fee,entry_fee_cash,entry_fee_token,"
				+ "prize_money,prize_money_cash,prize_money_token,per_of_winners,tot_fee,margin_amt,per_of_margin,"
				+ "break_even_at,percent_text,grid_footer_text,score_flag,max_participants,bypass_win_limit,reward_wallet_limit,championship_type,min_users from temp_tournament_formats where unq_id="
				+ formatId;
		try {
			return jdbcTemplate.queryForObject(SEL_QRY, new RowMapper<TournamentMaster>() {
				@Override
				public TournamentMaster mapRow(ResultSet rs, int i) throws SQLException {
					TournamentMaster tMaster;

					tMaster = new TournamentMaster();
					tMaster.setFormat(rs.getString("format"));
					tMaster.setFormatName(rs.getString("name"));
					tMaster.setFormatEntryFeeType(rs.getInt("entry_fee_type"));
					tMaster.setEntryFee(rs.getString("entry_fee"));
					tMaster.setEntryFeeCash(rs.getInt("entry_fee_cash"));
					tMaster.setEntryFeeToken(rs.getInt("entry_fee_token"));
					tMaster.setPrizeMoney(rs.getString("prize_money"));
					tMaster.setPrizeMoneyCash(rs.getInt("prize_money_cash"));
					tMaster.setPrizeMoneyToken(rs.getInt("prize_money_token"));
					tMaster.setPerOfWinners(rs.getInt("per_of_winners"));
					tMaster.setTotalFee(rs.getInt("tot_fee"));
					tMaster.setMarginAmount(rs.getInt("margin_amt"));
					tMaster.setPerOfMargin(rs.getFloat("per_of_margin"));
					tMaster.setBreakEvenAt(rs.getInt("break_even_at"));
					tMaster.setPercentText(rs.getString("percent_text"));
					tMaster.setGridFooterText(rs.getString("grid_footer_text"));
					tMaster.setScoreFlag(rs.getInt("score_flag"));
					tMaster.setMaxParticipants(rs.getInt("max_participants"));
					tMaster.setBypassWinLimit(rs.getInt("bypass_win_limit"));
					tMaster.setRewardWalletLimit(rs.getInt("reward_wallet_limit"));
					tMaster.setChampionshipType(rs.getInt("championship_type"));
					tMaster.setMin_users(rs.getInt("min_users"));

					return tMaster;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int saveTournamentDetails(TournamentMaster tMaster) {
		String INSERT_SQL = "INSERT INTO tournaments"
				+ "(game_id,name,users,prize_money,prize_money_cash,prize_money_token,entry_fee,"
				+ "entry_fee_cash,entry_fee_token,entry_fee_type,percent_text,status,start_time,end_time,"
				+ "added_date,created,modified,score_flag,"
				+ "format,format_name,format_entry_fee_type,per_of_winners,tot_fee,"
				+ "margin_amt,per_of_margin,break_even_at,grid_footer_text,is_active,max_participants,current_status,is_emergency,bypass_win_limit,reward_wallet_limit,temp_format_id,championship_type,t_level_id,min_users) VALUES "
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now(),?,?,?,?,?,?,?,?,?,?,'Y',?,?,?,?,?,?,?,?,?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplate.update(connection -> {
			PreparedStatement pstmt = connection.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, tMaster.getGameId());
			pstmt.setString(2, tMaster.getFormatName());
			pstmt.setInt(3, tMaster.getMaxParticipants());// users=max participants
			pstmt.setString(4, tMaster.getPrizeMoney());
			pstmt.setInt(5, tMaster.getPrizeMoneyCash());
			pstmt.setInt(6, tMaster.getPrizeMoneyToken());
			pstmt.setString(7, tMaster.getEntryFee());
			//
			pstmt.setInt(8, tMaster.getEntryFeeCash());
			pstmt.setInt(9, tMaster.getEntryFeeToken());
			pstmt.setInt(10, tMaster.getEntryFeeType());
			pstmt.setString(11, tMaster.getPercentText());
			pstmt.setInt(12, tMaster.getStatus());
			pstmt.setString(13, tMaster.gettStartTime());
			pstmt.setString(14, tMaster.gettEndTime());
			//
			pstmt.setString(15, tMaster.gettStartTime());
			pstmt.setInt(16, tMaster.getScoreFlag());
			//
			pstmt.setString(17, tMaster.getFormat());
			pstmt.setString(18, tMaster.getFormatName());
			pstmt.setInt(19, tMaster.getFormatEntryFeeType());
			pstmt.setInt(20, tMaster.getPerOfWinners());
			pstmt.setInt(21, tMaster.getTotalFee());
			pstmt.setInt(22, tMaster.getMarginAmount());
			pstmt.setFloat(23, tMaster.getPerOfMargin());
			pstmt.setInt(24, tMaster.getBreakEvenAt());
			pstmt.setString(25, tMaster.getGridFooterText());
			pstmt.setInt(26, tMaster.getMaxParticipants());
			pstmt.setInt(27, 1);
			pstmt.setInt(28, tMaster.getEmergencyTournment());// is emergency tournment
			pstmt.setInt(29, tMaster.getBypassWinLimit());
			pstmt.setInt(30, tMaster.getRewardWalletLimit());
			pstmt.setInt(31, tMaster.getFormatId());
			pstmt.setInt(32, tMaster.getChampionshipType());
			pstmt.setLong(33, tMaster.gettLevelId());
			pstmt.setInt(34, tMaster.getMin_users());

			return pstmt;
		}, keyHolder);
		return keyHolder.getKey().intValue();
	}

	@Override
	public List<RankingMaster> getRankingListByFormatId(int formatId) {

		List<RankingMaster> rankingList = null;
		String SEL_QRY = "SELECT ttf.format,frr.min_rank,frr.max_rank,frr.winners,frr.prize_money,frr.prize_money_cash,frr.prize_money_token,frr.min_score"
				+ " from temp_format_rank_range frr left join"
				+ " temp_tournament_formats ttf on (frr.temp_tourn_format_id=ttf.unq_id)"
				+ " where frr.is_active='Y' and frr.temp_tourn_format_id=" + formatId;

		try {
			rankingList = jdbcTemplate.query(SEL_QRY, new RowMapper<RankingMaster>() {
				@Override
				public RankingMaster mapRow(ResultSet rs, int i) throws SQLException {
					RankingMaster rankingMaster = new RankingMaster();
					rankingMaster.setMinRank(rs.getInt("min_rank"));
					rankingMaster.setMaxRank(rs.getInt("max_rank"));
					rankingMaster.setWinners(rs.getInt("winners"));
					rankingMaster.setPrizeMoney(rs.getString("prize_money"));
					rankingMaster.setPrizeMoneycash(rs.getInt("prize_money_cash"));
					rankingMaster.setPrizeMoneyToken(rs.getInt("prize_money_token"));
					rankingMaster.setFormatName(rs.getString("format"));
					rankingMaster.setMin_score(rs.getInt("min_score"));
					return rankingMaster;
				}
			});
		} catch (Exception e) {
			System.out.println("Exception occured in FormatDaoImpl(getRankingListByFormatId) ::" + e.getMessage());
		}
		return rankingList;
	}

	@Override
	public void saveRankingDetails(RankingMaster rm) {
		String INSERT_SQL = "insert into rank_ranges(tournament_id,max_rank,min_rank,prize_money,prize_money_cash,"
				+ "prize_money_token,status,added_date,created,min_score) values (?,?,?,?,?,?,?,now(),now(),?)";
		jdbcTemplate.update(INSERT_SQL, new Object[] { rm.getTournamentId(), rm.getMaxRank(), rm.getMinRank(),
				rm.getPrizeMoney(), rm.getPrizeMoneycash(), rm.getPrizeMoneyToken(), 1, rm.getMin_score() });
	}

	@Override
	public void createTournment(int gameId, int formatId, int entryFeeType, String startTime, String endTime) {
		int tournamentId;
		TournamentMaster tMaster;
		try {
			tMaster = getFormatDetails(formatId);
			if (tMaster != null) {
				tMaster.setGameId(gameId);
				tMaster.setEntryFeeType(entryFeeType);
				tMaster.setFormatId(formatId);
				tMaster.settStartTime(startTime);
				tMaster.settEndTime(endTime);
				tMaster.setEmergencyTournment(1);
				tMaster.setBypassWinLimit(0);
				tMaster.setRewardWalletLimit(-1);
				tMaster.setChampionshipType(0);

				if (tMaster.getGameId() > 0) {

					LOGS.info("::TOURNMENT CREATION HAPPENING::");
					LOGS.info("gameId::" + gameId + "::formatId::" + formatId + "::entryFeeType::" + entryFeeType
							+ "::startTime::" + startTime + "::endTime::" + endTime);

					tournamentId = saveTournamentDetails(tMaster);
					if (tournamentId > 0) {
						List<RankingMaster> rankingMasterList = getRankingListByFormatId(tMaster.getFormatId());
						for (Iterator iterator = rankingMasterList.iterator(); iterator.hasNext();) {
							RankingMaster rm = (RankingMaster) iterator.next();
							rm.setTournamentId(tournamentId);
							saveRankingDetails(rm);
						}
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean checkGameActive(int gameId) {
		return jdbcTemplate.queryForObject("select count(1) from games where id=? and status=1 ",
				new Object[] { gameId }, Integer.class) > 0;
	}

	boolean checkTournmentsFilled(int gameId, int entryFeeType) {

		String query = "SELECT count(1) FROM tournaments where game_id=? and entry_fee_type=? and start_time<now() and end_time>now() and status not in (2,3) and is_full=0";

		return jdbcTemplate.queryForObject(query, new Object[] { gameId, entryFeeType }, Integer.class) == 0;

	}

	@Override
	public List<AutoTournmentMaster> getListAutoTournments(int gameId) {

		return jdbcTemplate.query(
				"SELECT id,game_id,entry_fee_type,format_id,t_level_id,tournament_duration FROM auto_create_championship where game_id="
						+ gameId + " and is_active=1 and current_time between start_time and end_time",
				new RowMapper<AutoTournmentMaster>() {
					@Override
					public AutoTournmentMaster mapRow(ResultSet rs, int i) throws SQLException {
						AutoTournmentMaster autoTournmentMaster = new AutoTournmentMaster();

						autoTournmentMaster.setId(rs.getInt("id"));
						autoTournmentMaster.setGameId(rs.getInt("game_id"));
						autoTournmentMaster.setEntreeFeeType(rs.getInt("entry_fee_type"));
						autoTournmentMaster.setFormatId(rs.getInt("format_id"));
						autoTournmentMaster.settLevelId(rs.getLong("t_level_id"));
						autoTournmentMaster.setTournament_duration(rs.getInt("tournament_duration"));
						return autoTournmentMaster;
					}
				});

	}

	@Override
	public LiveTournmentMaster getLastLiveTournmentByGameIdAndFormatId(int gameId, int formatId, int entryFeeType,
			long tLevelId) {

		String query = "SELECT id,TIMESTAMPDIFF(MINUTE, start_time, end_time) as tDuration,now()>end_time as tCompleted,is_full as isFull,t_level_id,champ_state,min_users FROM tournaments where game_id=? and temp_format_id=? and entry_fee_type=? and t_level_id=? and start_time<=now() and is_active='Y' and champ_state=0 order by start_time desc,end_time desc limit 1";

		return jdbcTemplate.query(query, new Object[] { gameId, formatId, entryFeeType, tLevelId },
				new ResultSetExtractor<LiveTournmentMaster>() {

					@Override
					public LiveTournmentMaster extractData(ResultSet rs) throws SQLException, DataAccessException {
						LiveTournmentMaster liveTournmentMaster = null;
						if (rs.next()) {
							liveTournmentMaster = new LiveTournmentMaster();
							liveTournmentMaster.setTournament_id(rs.getInt("id"));
							liveTournmentMaster.setGameId(gameId);
							liveTournmentMaster.setIsFull(rs.getInt("isFull"));
							liveTournmentMaster.settCompleted(rs.getInt("tCompleted"));
							liveTournmentMaster.settDuration(rs.getInt("tDuration"));
							liveTournmentMaster.settLevelId(rs.getLong("t_level_id"));
							liveTournmentMaster.setChamp_state(rs.getInt("champ_state"));
							liveTournmentMaster.setMin_users(rs.getInt("min_users"));

						}

						return liveTournmentMaster;
					}

				});

	}

	@Override
	public void createAutoTournment(TournamentMaster tMaster) {
		int tournamentId;
		try {
			LOGS.info("::Auto TOURNMENT CREATION HAPPENING::");
			tournamentId = saveTournamentDetails(tMaster);
			if (tournamentId > 0) {
				List<RankingMaster> rankingMasterList = getRankingListByFormatId(tMaster.getFormatId());
				for (Iterator<RankingMaster> iterator = rankingMasterList.iterator(); iterator.hasNext();) {
					RankingMaster rm = (RankingMaster) iterator.next();
					rm.setTournamentId(tournamentId);
					saveRankingDetails(rm);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public int getTournamentRegistrationCount(long tournament_id) {
		LOGS.info("Gettting Data for: {}", tournament_id);
		Query query;
		try {
			query = new Query();
			query.addCriteria(Criteria.where("tournament_id").is(tournament_id));
			return (int) mongoTemplate.count(query, "tournament_registration");

		} catch (Exception e) {
			LOGS.info("Exception occured in getTournamentRegistrationCount::" + e.getMessage());
		}
		return 0;
	}

}
