package com.games.properties;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TournamentProperties {

	@Value("#{'${auto.game.service.games}'.split(',')}")
	private List<Integer> autoGameServiceGames;

	@Value("#{'${auto.game.service.games.one}'.split(',')}")
	private List<Integer> autoGameServiceGamesOne;

	@Value("#{'${auto.game.service.games.two}'.split(',')}")
	private List<Integer> autoGameServiceGamesTwo;

	public List<Integer> getAutoGameServiceGames() {
		return autoGameServiceGames;
	}

	public void setAutoGameServiceGames(List<Integer> autoGameServiceGames) {
		this.autoGameServiceGames = autoGameServiceGames;
	}

	public List<Integer> getAutoGameServiceGamesOne() {
		return autoGameServiceGamesOne;
	}

	public void setAutoGameServiceGamesOne(List<Integer> autoGameServiceGamesOne) {
		this.autoGameServiceGamesOne = autoGameServiceGamesOne;
	}

	public List<Integer> getAutoGameServiceGamesTwo() {
		return autoGameServiceGamesTwo;
	}

	public void setAutoGameServiceGamesTwo(List<Integer> autoGameServiceGamesTwo) {
		this.autoGameServiceGamesTwo = autoGameServiceGamesTwo;
	}

}
