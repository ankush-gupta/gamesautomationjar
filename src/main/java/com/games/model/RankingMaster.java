package com.games.model;

import java.util.List;

public class RankingMaster {
	private String formatName;
	private int minRank;
	private int maxRank;
	private int winners;
	private String prizeMoney;
	private int prizeMoneycash;
	private int prizeMoneyToken;
	private int formatId;
	private int tournamentId;
	private int min_score;
	public int getMin_score() {
		return min_score;
	}

	public void setMin_score(int min_score) {
		this.min_score = min_score;
	}

	private int gameId;
	private String tStartDate;

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String gettStartDate() {
		return tStartDate;
	}

	public void settStartDate(String tStartDate) {
		this.tStartDate = tStartDate;
	}

	public int getTournamentId() {
		return tournamentId;
	}

	public void setTournamentId(int tournamentId) {
		this.tournamentId = tournamentId;
	}

	public String getFormatName() {
		return formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

	private List<RankingMaster> rankingList;

	public List<RankingMaster> getRankingList() {
		return rankingList;
	}

	public void setRankingList(List<RankingMaster> rankingList) {
		this.rankingList = rankingList;
	}

	public int getFormatId() {
		return formatId;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

	public int getMinRank() {
		return minRank;
	}

	public void setMinRank(int minRank) {
		this.minRank = minRank;
	}

	public int getMaxRank() {
		return maxRank;
	}

	public void setMaxRank(int maxRank) {
		this.maxRank = maxRank;
	}

	public int getWinners() {
		return winners;
	}

	public void setWinners(int winners) {
		this.winners = winners;
	}

	public String getPrizeMoney() {
		return prizeMoney;
	}

	public void setPrizeMoney(String prizeMoney) {
		this.prizeMoney = prizeMoney;
	}

	public int getPrizeMoneycash() {
		return prizeMoneycash;
	}

	public void setPrizeMoneycash(int prizeMoneycash) {
		this.prizeMoneycash = prizeMoneycash;
	}

	public int getPrizeMoneyToken() {
		return prizeMoneyToken;
	}

	public void setPrizeMoneyToken(int prizeMoneyToken) {
		this.prizeMoneyToken = prizeMoneyToken;
	}

}
