package com.games.model;

import java.util.List;

public class TournamentMaster {
	private int gameId;
	private String gameName;
	private int users;
	private int formatId;
	private String tStartDate;
	private String tStartTime;
	private String tEndTime;
	private int tournamentId;
	private long tLevelId;
	private int min_users;

	public long gettLevelId() {
		return tLevelId;
	}

	public void settLevelId(long tLevelId) {
		this.tLevelId = tLevelId;
	}

	public int getMin_users() {
		return min_users;
	}

	public void setMin_users(int min_users) {
		this.min_users = min_users;
	}

	//
	private String prizeMoney;
	private int prizeMoneyCash;
	private int prizeMoneyToken;
	//

	private String formatName;
	private String format;
	private String percentText;
	private String gridFooterText;
	//
	private String entryFee;
	private int entryFeeCash;
	private int entryFeeToken;
	//
	private int entryFeeType;
	private int formatEntryFeeType;
	//
	private int status;
	private int currentStatus;
	private int scoreFlag;
	private int breakEvenAt;
	private float perOfMargin;
	private int marginAmount;
	private int totalFee;
	private int perOfWinners;
	private String formType;
	private List<RankingMaster> rankingList;
	private int hours;
	private int minutes;
	private int maxParticipants;
	private int emergencyTournment;
	private int bypassWinLimit;
	private int championshipType;

	public int getChampionshipType() {
		return championshipType;
	}

	public void setChampionshipType(int championshipType) {
		this.championshipType = championshipType;
	}

	public int getBypassWinLimit() {
		return bypassWinLimit;
	}

	public int getRewardWalletLimit() {
		return rewardWalletLimit;
	}

	public void setBypassWinLimit(int bypassWinLimit) {
		this.bypassWinLimit = bypassWinLimit;
	}

	public void setRewardWalletLimit(int rewardWalletLimit) {
		this.rewardWalletLimit = rewardWalletLimit;
	}

	private int rewardWalletLimit;

	public int getEmergencyTournment() {
		return emergencyTournment;
	}

	public void setEmergencyTournment(int emergencyTournment) {
		this.emergencyTournment = emergencyTournment;
	}

	public int getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(int currentStatus) {
		this.currentStatus = currentStatus;
	}

	public int getMaxParticipants() {
		return maxParticipants;
	}

	public void setMaxParticipants(int maxParticipants) {
		this.maxParticipants = maxParticipants;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public List<RankingMaster> getRankingList() {
		return rankingList;
	}

	public void setRankingList(List<RankingMaster> rankingList) {
		this.rankingList = rankingList;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public int getUsers() {
		return users;
	}

	public void setUsers(int users) {
		this.users = users;
	}

	public int getFormatId() {
		return formatId;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

	public String gettStartDate() {
		return tStartDate;
	}

	public void settStartDate(String tStartDate) {
		this.tStartDate = tStartDate;
	}

	public String gettStartTime() {
		return tStartTime;
	}

	public void settStartTime(String tStartTime) {
		this.tStartTime = tStartTime;
	}

	public String gettEndTime() {
		return tEndTime;
	}

	public void settEndTime(String tEndTime) {
		this.tEndTime = tEndTime;
	}

	public int getTournamentId() {
		return tournamentId;
	}

	public void setTournamentId(int tournamentId) {
		this.tournamentId = tournamentId;
	}

	public String getPrizeMoney() {
		return prizeMoney;
	}

	public void setPrizeMoney(String prizeMoney) {
		this.prizeMoney = prizeMoney;
	}

	public int getPrizeMoneyCash() {
		return prizeMoneyCash;
	}

	public void setPrizeMoneyCash(int prizeMoneyCash) {
		this.prizeMoneyCash = prizeMoneyCash;
	}

	public int getPrizeMoneyToken() {
		return prizeMoneyToken;
	}

	public void setPrizeMoneyToken(int prizeMoneyToken) {
		this.prizeMoneyToken = prizeMoneyToken;
	}

	public String getFormatName() {
		return formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getPercentText() {
		return percentText;
	}

	public void setPercentText(String percentText) {
		this.percentText = percentText;
	}

	public String getGridFooterText() {
		return gridFooterText;
	}

	public void setGridFooterText(String gridFooterText) {
		this.gridFooterText = gridFooterText;
	}

	public String getEntryFee() {
		return entryFee;
	}

	public void setEntryFee(String entryFee) {
		this.entryFee = entryFee;
	}

	public int getEntryFeeCash() {
		return entryFeeCash;
	}

	public void setEntryFeeCash(int entryFeeCash) {
		this.entryFeeCash = entryFeeCash;
	}

	public int getEntryFeeToken() {
		return entryFeeToken;
	}

	public void setEntryFeeToken(int entryFeeToken) {
		this.entryFeeToken = entryFeeToken;
	}

	public int getEntryFeeType() {
		return entryFeeType;
	}

	public void setEntryFeeType(int entryFeeType) {
		this.entryFeeType = entryFeeType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getScoreFlag() {
		return scoreFlag;
	}

	public void setScoreFlag(int scoreFlag) {
		this.scoreFlag = scoreFlag;
	}

	public int getBreakEvenAt() {
		return breakEvenAt;
	}

	public void setBreakEvenAt(int breakEvenAt) {
		this.breakEvenAt = breakEvenAt;
	}

	public float getPerOfMargin() {
		return perOfMargin;
	}

	public void setPerOfMargin(float perOfMargin) {
		this.perOfMargin = perOfMargin;
	}

	public int getMarginAmount() {
		return marginAmount;
	}

	public void setMarginAmount(int marginAmount) {
		this.marginAmount = marginAmount;
	}

	public int getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(int totalFee) {
		this.totalFee = totalFee;
	}

	public int getPerOfWinners() {
		return perOfWinners;
	}

	public void setPerOfWinners(int perOfWinners) {
		this.perOfWinners = perOfWinners;
	}

	public int getFormatEntryFeeType() {
		return formatEntryFeeType;
	}

	public void setFormatEntryFeeType(int formatEntryFeeType) {
		this.formatEntryFeeType = formatEntryFeeType;
	}

}
