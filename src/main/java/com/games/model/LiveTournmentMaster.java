package com.games.model;

public class LiveTournmentMaster {

	private int gameId;
	private int formatId;
	
	private int isFull;
	private int tCompleted;
	private int tDuration;
	private long tLevelId;
	private int champ_state;
	private int min_users;

	private int tournament_id;

	public int getTournament_id() {
		return tournament_id;
	}

	public void setTournament_id(int tournament_id) {
		this.tournament_id = tournament_id;
	}

	public int getMin_users() {
		return min_users;
	}

	public void setMin_users(int min_users) {
		this.min_users = min_users;
	}

	public int getChamp_state() {
		return champ_state;
	}

	public void setChamp_state(int champ_state) {
		this.champ_state = champ_state;
	}

	

	public long gettLevelId() {
		return tLevelId;
	}

	public void settLevelId(long tLevelId) {
		this.tLevelId = tLevelId;
	}

	public int getGameId() {
		return gameId;
	}

	public int getFormatId() {
		return formatId;
	}

	

	public int getIsFull() {
		return isFull;
	}

	public int gettCompleted() {
		return tCompleted;
	}

	public int gettDuration() {
		return tDuration;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

	

	public void setIsFull(int isFull) {
		this.isFull = isFull;
	}

	public void settCompleted(int tCompleted) {
		this.tCompleted = tCompleted;
	}

	public void settDuration(int tDuration) {
		this.tDuration = tDuration;
	}

}
