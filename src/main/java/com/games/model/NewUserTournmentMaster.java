package com.games.model;

import java.util.List;

public class NewUserTournmentMaster {

	private int gameId;
	private String name;
	private int users;
	private String prizeMoney;
	private int prizeMoneyCash;
	private int prizeMoneyToken;

	private String entryFee;
	private int entryFeeCash;
	private int entryFeeToken;
	private int entryFeeType;
	private int tournmentDurationMints;

	private String percentText;
	private List<Integer> maxRank;
	private List<Integer> minRank;
	private List<String> rankPrizeMoney;
	private List<Integer> rankPrizeMoneyCash;
	private List<Integer> rankprizeMoneyToken;

	public int getGameId() {
		return gameId;
	}

	public String getName() {
		return name;
	}

	public int getUsers() {
		return users;
	}

	public String getPrizeMoney() {
		return prizeMoney;
	}

	public int getPrizeMoneyCash() {
		return prizeMoneyCash;
	}

	public int getPrizeMoneyToken() {
		return prizeMoneyToken;
	}

	public String getEntryFee() {
		return entryFee;
	}

	public int getEntryFeeCash() {
		return entryFeeCash;
	}

	public int getEntryFeeToken() {
		return entryFeeToken;
	}

	public int getEntryFeeType() {
		return entryFeeType;
	}

	public String getPercentText() {
		return percentText;
	}

	public List<Integer> getMaxRank() {
		return maxRank;
	}

	public List<Integer> getMinRank() {
		return minRank;
	}

	public List<String> getRankPrizeMoney() {
		return rankPrizeMoney;
	}

	public List<Integer> getRankPrizeMoneyCash() {
		return rankPrizeMoneyCash;
	}

	public List<Integer> getRankprizeMoneyToken() {
		return rankprizeMoneyToken;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUsers(int users) {
		this.users = users;
	}

	public void setPrizeMoney(String prizeMoney) {
		this.prizeMoney = prizeMoney;
	}

	public void setPrizeMoneyCash(int prizeMoneyCash) {
		this.prizeMoneyCash = prizeMoneyCash;
	}

	public void setPrizeMoneyToken(int prizeMoneyToken) {
		this.prizeMoneyToken = prizeMoneyToken;
	}

	public void setEntryFee(String entryFee) {
		this.entryFee = entryFee;
	}

	public void setEntryFeeCash(int entryFeeCash) {
		this.entryFeeCash = entryFeeCash;
	}

	public void setEntryFeeToken(int entryFeeToken) {
		this.entryFeeToken = entryFeeToken;
	}

	public void setEntryFeeType(int entryFeeType) {
		this.entryFeeType = entryFeeType;
	}

	public void setPercentText(String percentText) {
		this.percentText = percentText;
	}

	public void setMaxRank(List<Integer> maxRank) {
		this.maxRank = maxRank;
	}

	public void setMinRank(List<Integer> minRank) {
		this.minRank = minRank;
	}

	public void setRankPrizeMoney(List<String> rankPrizeMoney) {
		this.rankPrizeMoney = rankPrizeMoney;
	}

	public void setRankPrizeMoneyCash(List<Integer> rankPrizeMoneyCash) {
		this.rankPrizeMoneyCash = rankPrizeMoneyCash;
	}

	public void setRankprizeMoneyToken(List<Integer> rankprizeMoneyToken) {
		this.rankprizeMoneyToken = rankprizeMoneyToken;
	}

	public int getTournmentDurationMints() {
		return tournmentDurationMints;
	}

	public void setTournmentDurationMints(int tournmentDurationMints) {
		this.tournmentDurationMints = tournmentDurationMints;
	}

}
