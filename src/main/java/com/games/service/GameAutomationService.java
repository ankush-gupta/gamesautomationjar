package com.games.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.games.dao.TournamentDao;
import com.games.model.AutoTournmentMaster;
import com.games.model.LiveTournmentMaster;
import com.games.model.TournamentMaster;
import com.games.util.CommonConstants;

@Service
public class GameAutomationService {
	private static final Logger LOGS = LoggerFactory.getLogger(GameAutomationService.class);

	@Autowired
	TournamentDao tournamentDao;

	@Async("gameAutomationAsyncExec")
	CompletableFuture<String> validateAndCreateTournament(AutoTournmentMaster autoTournmentMaster) {
		String startTime;
		String endTime;
		Calendar calendar;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		LiveTournmentMaster liveTournmentMaster;
		int tournmentDuration = 0;
		TournamentMaster tournamentMaster;
		try {
			if (tournamentDao.checkGameActive(autoTournmentMaster.getGameId())) {

				liveTournmentMaster = tournamentDao.getLastLiveTournmentByGameIdAndFormatId(
						autoTournmentMaster.getGameId(), autoTournmentMaster.getFormatId(),
						autoTournmentMaster.getEntreeFeeType(), autoTournmentMaster.gettLevelId());

				if (liveTournmentMaster == null || liveTournmentMaster.getIsFull() == 1
						|| liveTournmentMaster.gettCompleted() == 1) {
					// create a tournment

					tournamentMaster = tournamentDao.getFormatDetails(autoTournmentMaster.getFormatId());
					if (tournamentMaster != null) {
						tournamentMaster.setGameId(autoTournmentMaster.getGameId());
						tournamentMaster.setEntryFeeType(autoTournmentMaster.getEntreeFeeType());
						tournamentMaster.setFormatId(autoTournmentMaster.getFormatId());
						tournamentMaster.settLevelId(autoTournmentMaster.gettLevelId());
						tournamentMaster.setEmergencyTournment(0);

						tournmentDuration = autoTournmentMaster.getTournament_duration();

						if (liveTournmentMaster != null) {
							if (liveTournmentMaster.getIsFull() == 1) {
								// default tournament duration
							} else {
								// if tournement completed and not full
								if ((liveTournmentMaster.getChamp_state() == CommonConstants.CHAMP_STATE_CANCELLED)
										|| (tournamentDao.getTournamentRegistrationCount(
												liveTournmentMaster.getTournament_id()) < liveTournmentMaster
														.getMin_users())) {
									tournmentDuration = 2 * autoTournmentMaster.getTournament_duration();
									if (tournmentDuration > 60) {
										tournmentDuration = 60;
									}
								}
							}
						}

						calendar = Calendar.getInstance();

						startTime = sdf.format(calendar.getTime()) + " " + String.format("%02d:%02d:%02d",
								calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);

						calendar.add(Calendar.MINUTE, tournmentDuration);

						endTime = sdf.format(calendar.getTime()) + " " + String.format("%02d:%02d:%02d",
								calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);

						tournamentMaster.settStartTime(startTime);
						tournamentMaster.settEndTime(endTime);

						tournamentDao.createAutoTournment(tournamentMaster);

					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGS.info("Exception occured in GameAutomationService(validateAndCreateTournament)::" + e.getMessage());
		}
		return CompletableFuture.completedFuture("processed gid ::" + autoTournmentMaster.getGameId() + ":::fid::"
				+ autoTournmentMaster.getFormatId() + ":::efee::" + autoTournmentMaster.getEntreeFeeType() + ":::lid::"
				+ autoTournmentMaster.gettLevelId());
	}

}
