package com.games.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games.dao.TournamentDao;
import com.games.model.AutoTournmentMaster;
import com.games.model.NewUserTournmentMaster;
import com.games.properties.TournamentProperties;

@Service
public class GameService {
	private static final Logger LOGS = LoggerFactory.getLogger(GameService.class);

	@Autowired
	GameAutomationService gameAutomationService;

	@Autowired
	TournamentDao tournamentDao;

	@Autowired
	TournamentProperties tournamentProperties;

	public void generateAutoGames(int gameId) {
		List<AutoTournmentMaster> listAutoTournments;
		List<CompletableFuture<String>> listComletableFuture;
		try {

			listComletableFuture = new ArrayList<>();
			listAutoTournments = tournamentDao.getListAutoTournments(gameId);

			for (AutoTournmentMaster autoTournmentMaster : listAutoTournments) {
				listComletableFuture.add(gameAutomationService.validateAndCreateTournament(autoTournmentMaster));
			}

			for (int i = 0; i < listComletableFuture.size(); i++) {
				listComletableFuture.get(i).join();
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGS.info("Exception occured  in generateAutoGames::" + e.getMessage());
		}

	}

}
