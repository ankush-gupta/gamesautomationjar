package com.games.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import com.games.properties.TournamentProperties;
import com.games.service.GameService;

@Controller
public class CommonServiceStartController {

	private static final Logger logs = LoggerFactory.getLogger(CommonServiceStartController.class);

	@Autowired
	GameService gameService;

	@Autowired
	private TournamentProperties tournamentProperties;

	@Scheduled(fixedDelay = 10 * 1000)
	public void autoGameService() {

		/*
		 * fruit slash--5 don't crash--2 save panda --4
		 */
		// int[] listGameIds = { 5, 2, 4, 12, 21 };
		List<Integer> listGameIds = tournamentProperties.getAutoGameServiceGames();
		logs.info("::AUTO TOURMENTS SCHEDULER STARTED::" + listGameIds);

		for (int gameId : listGameIds) {
			gameService.generateAutoGames(gameId);
		}

	}

	/* temporary services for creating auto tournments */

	@Scheduled(fixedDelay = 10 * 1000)
	public void autoGameService_1() {

		/*
		 * CricketT20--10 Candy Burst--6 Earth Hero --1 RiseUp--13
		 */
		// int[] listGameIds = { 10, 6, 1, 13, 17, 22 };
		List<Integer> listGameIds = tournamentProperties.getAutoGameServiceGamesOne();
		logs.info("::AUTO TOURMENTS SCHEDULER STARTED_1::" + listGameIds);
		for (int gameId : listGameIds) {
			gameService.generateAutoGames(gameId);
		}
	}

	@Scheduled(fixedDelay = 10 * 1000)
	public void autoGameService_2() {
		/*
		 * Bubble Shooter--8 Bike Climb--9 Bhaag Ninja--3 Happy Jump--11
		 */

		// logs.info("::AUTO TOURMENTS SCHEDULER STARTED::");
		// int[] listGameIds = { 7, 8, 11, 19, 20 };
		List<Integer> listGameIds = tournamentProperties.getAutoGameServiceGamesTwo();
		logs.info("::AUTO TOURMENTS SCHEDULER STARTED_2::" + listGameIds);
		for (int gameId : listGameIds) {
			gameService.generateAutoGames(gameId);
		}
	}

}
